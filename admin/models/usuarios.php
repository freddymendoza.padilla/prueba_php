<?php 
//require_once('../models/ConfigDB.php');
   include('../../config/init_db.php');
class Usuarios 
{

      /*
        * Autor: freddy mendoza
        * Email: freddymendoza.padilla1@gmail.com
        * Descripción: Metodo que permite listar todos los usuarios
        * Fecha Creación: 2021-07-23
    */
    public static function MdlListar()
    {
       include('../../config/init_db.php');
        DB::$encoding = 'utf8';
        $query  = "SELECT * FROM test_users";
        $result = DB::query($query);
        return $result;

        DB::disconnect();
    }



    public static function mdlRegistar($p){
        extract($p);
        try {
            
          $query = (" INSERT INTO test_users(
                                                            full_name,
                                                            email,
                                                            celular,
                                                            name_folder,
                                                            date
                                                            )
                                                            VALUES(
                                                            '{$nombres}',
                                                            '{$email}',
                                                            $Telefono,
                                                            '{$carpeta}',
                                                            NOW()
                                                            )");
             DB::query($query);
            $data['error']    = false;
            $data['mensaje']  = 'usuario creado correctamente';
            
            
        } catch(MeekroDBException $e) {
             echo "Error: " . $e->getMessage() . "<br>\n";
             echo "SQL Query: " . $e->getQuery() . "<br>\n";
            $data['error']    = true;
            $data['mensaje']  = 'error al crear usuario';
        }
        return $data;
    }

    public static function mdlEditar($d){
        try {
            DB::query("");
            $data['error'] = false;
            $data['mensaje'] = 'Información editada correctamente';
        } catch(MeekroDBException $e) {
            $data['error'] = true;
            $data['mensaje'] = 'Error, no se logro editar la información';
        }
        return $data;
        
    }
}