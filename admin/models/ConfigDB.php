<?php

class Conexion
{
    /*
        * Autor: freddy mendoza
        * Email: freddymendoza.padilla1@gmail.com
        * Descripción: Metodo que permite hacer la conexion a la base datos por medio de PDO 
        * Fecha Creación: 2021-07-23
    */
    public static function conectar()
    {
        $conexion = new PDO("mysql:host=localhost;dbname=test_developers", "root", "");
        $conexion->exec("set names utf8");
        return $conexion;
    }
}