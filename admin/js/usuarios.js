$(document).ready(function() {
  listar_usuarios()
});


/* listar usuarios*/

function listar_usuarios(){

  $.ajax({
    url: 'controllers/usuarios.php',
    type: 'POST',
    dataType: 'JSON',
    data: {opcn: 'ctrListar'},
  })
  .done(function(data) {
    console.log(data);
    let table;
    let opcion = ''
    for (var i = 0; i < data.length; i++) {
      for (var i = 0; i < data.length; i++) {
      table+='<tr>'
      table+='<td>'+data[i].id+'</td>'
      table+='<td>'+data[i].full_name+'</td>'
      table+='<td>'+data[i].email+'</td>'
      table+='<td>'+data[i].celular+'</td>'
      table+='<td>'+data[i].name_folder+'</td>'
      table+='<td>'+data[i].date+'</td>'
      table+='</tr>'
    }
    jQuery('#tabla_usaurios tbody').html(table)
      jQuery('#tabla_usaurios').DataTable({
                    "language": {
                      "sProcessing": "Procesando...",
                      "sLengthMenu": "",
                      "sZeroRecords": "No se encontraron resultados",
                      "sEmptyTable": "NingÂ¨Â²n dato disponible en esta tabla",
                      "sInfo": "Mostrando registros del Inicio al Final de un total de TOTAL registros",
                      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered": "(de MAX registros)",
                      "sInfoPostFix": "",
                      "sSearch": "Buscar:",
                      "sUrl": "",
                      "sInfoThousands": ",",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                      "sFirst": "Primero",
                      "sLast": "Â0â€°3ltimo",
                      "sNext": "Siguiente",
                      "sPrevious": "Anterior"
                      },
                      "oAria": {
                      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                      }
                      ,
                      dom: 'lBfrtip',
                      pageLength: 12,
                      buttons: [
                      {extend : 'excelHtml5',
                      text: '<i class="las la-file-excel">EXCEL</i>',
                      filename: 'clientes_autotrain',
                      },
                      {extend : 'pdfHtml5',
                      text: '<i class="las la-file-pdf">PDF</i>',
                      //orientation: 'landscape',
                      filename: 'clientes_autotrain',
                      exportOptions: {
                       columns: [0,1,2,3,4,5],
                      }
                      },
                      {extend: 'print',text: '<i class="las la-print">IMPRIMIR</i>', filename: 'clientes_autotrain'}
                      ]
             });//convierte en datatable

    }
   
  })
  .fail(function() {
    console.log("error");
  })
  
}

/* insertar usuarios*/

$('#add_usuarios').submit(function(event) {
  event.preventDefault();
  let info = $(this).serializeArray();
  
    info.push({name:'opcn',value:'ctrCrear'});
    $.ajax({
      url: 'controllers/usuarios.php',
      type: 'POST',
      dataType: 'JSON',
      data: info,
    })
    .done(function(data) {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    
    
 });