<div id="addusuarios" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="add_usuarios" id="add_usuarios">
					<div class="modal-header">						
						<h4 class="modal-title">Agregar usuarios</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Nombres</label>
							<input type="text" name="nombres"  id="nombres" class="form-control" required>
						
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" name="email"  class="form-control " id="email" placeholder="email" required>
							
						</div>
						<div class="form-group">
							<label>Telefono</label>
							<input type="text" name="Telefono" id="Telefono" class="form-control" required>
						</div>
						<div class="form-group">
							<label>carpeta</label>
							<input type="text" name="carpeta" id="carpeta" class="form-control" required>
						</div>
									
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Guardar datos">
					</div>
				</form>
			</div>
		</div>
	</div>