<?php

$metodo = $_SERVER['REQUEST_METHOD'];
switch ($metodo) {
    case 'POST':
        include_once('../models/usuarios.php');
        switch ($_POST['opcn']) {
            case 'ctrListar':
                $info = Usuarios::mdlListar();
                echo json_encode($info);
                break;
            case 'ctrCrear':
                $info = Usuarios::mdlRegistar($_POST);
                echo json_encode($info);
                break;
            case 'ctrEditar':
                $info = Usuarios::mdlEditar($_POST);
                echo json_encode($info);
                break;
        }
        break;
}