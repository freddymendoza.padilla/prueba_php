<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Prueba_php</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
				<div class="row">
                    <div class="col-sm-6">
						<h2></b></h2>
					</div>
					<div class="col-sm-6">
						<a href="#addusuarios" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar nuevo Usuario</span></a>
					</div>
                </div>
	<div class="cuerpo">
					<h3 style="text-align: center;
					">Reporte de Inscritos</h3>
					<div class="row">
						<div class="col-xl-1"></div>

						<div class="col-xl-10">
							<div style="text-align: center;">
								<span>Lista de usuarios</span> 
							</div>
							
							 <table id="tabla_usaurios" class="display table table-striped table-bordered table-condensed table-vertical-center js-table-sortable table-hover" style="text-transform: uppercase;text-align: center;">
								
								<thead>
									<tr>
										<th >ID</th>
										<th >Nombres</th>
		                                <th >Email</th>
		                                <th >Teléfono</th>
		                                <th >Carpeta</th>
		                                <th >Fecha</th>
		                                
									</tr>
								</thead>
								<tbody>
									<div style="text-align: center;"><span id="num_usuarios1" style="text-align: center;"></span></div>
								</tbody>
								
							</table>
								
						</div>
						<div class="col-xl-1"></div>
					</div>
				</div>
<!-- add Modal HTML -->
	<?php include("modales/modal_add.php");?>

 <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js" type="text/javascript" ></script>
<script src="js/usuarios.js"></script>
</body>
</html>